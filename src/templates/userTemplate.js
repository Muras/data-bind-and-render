export default `
  <div>
    <h2 data-testid="fullNameBind" data-bind="personalData.fullName" data-bind-title="title"></h2>

    <div data-testid="streetTitle" data-bind-title="address.street">
      Ulica: <span data-testid="streetBind" data-bind="address.street"></span>
    </div>
    <div data-testid="contactSrcHref" data-bind-src="address.contact.tel" data-bind-href="personalData.fullName">
      Telefon: <span data-testid="telBind "data-bind="address.contact.tel"></span>
    </div>
  </div>
`;