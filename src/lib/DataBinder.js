import { onObjectChange } from '../utils/objectObservers';
import { getObjectValueByDotNotation, is } from '../utils/dataHelpers';
import { bindableAttributes, datasetToAttributesMap } from '../utils/constants';
import { viewUpdateEvent } from '../utils/customEvents';


class DataBinder {
  bind(model, viewTemplate) {
    const resultView = this.generateView(
      model,
      viewTemplate
    );
    
    this.attachViewUpdateEvent(model, viewTemplate, resultView);

    this.setupWatcher(model, resultView);

    return resultView;
  }

  setupWatcher(model, resultView) {
    onObjectChange(model, () => {
      resultView.dispatchEvent(viewUpdateEvent);
    });
  }

  attachViewUpdateEvent(model, viewTemplate, resultView) {
    resultView.addEventListener('view_update', (event) => {
      event.target.innerHTML = this.generateView(
        model,
        viewTemplate
      ).innerHTML;
    });
  }

  generateView(model, viewTemplate) {
    const nodeRoot = document.createElement('div');
    nodeRoot.innerHTML = viewTemplate;
    const dataFragmentsRef = nodeRoot.querySelectorAll('*') || [];

    dataFragmentsRef.forEach(this.setNodeValues(model))

    return nodeRoot.children[0]; // I had issues while working with Range.createContextualFragment
  }

  setNodeValues(model) {
    return function setNodeValues(nodeRef) {
      const { dataset = {} } = nodeRef;

      if (dataset.bind) {
        nodeRef.innerText = getObjectValueByDotNotation(dataset.bind, model);
      }

      Object.entries(dataset).forEach(([dataAttribute, value]) => {
        if (bindableAttributes.includes(dataAttribute)) {
          const datasetValue = getObjectValueByDotNotation(value, model);

          if (!is.notExisting(datasetValue)) {
            nodeRef.setAttribute(
              datasetToAttributesMap[dataAttribute],
              datasetValue
            );
          }
        }
      });
    }
  }
}

export default DataBinder;
