import domEvents from './domEvents'
import DataBinder from './lib/DataBinder';
import schibModel from './models/schibModel';
import userModel from './models/userModel';
import schibTemplate from './templates/schibTemplate';
import userTemplate from './templates/userTemplate';

const appNode = document.getElementById('app');
const changeSchibModelButton = document.getElementById('change_schib_model');
const changeUserModelButton = document.getElementById('change_user_model');

const dataBinder = new DataBinder();

const schibView = dataBinder.bind(schibModel, schibTemplate);
const userView = dataBinder.bind(userModel, userTemplate);

appNode.appendChild(schibView);
appNode.appendChild(userView);

changeSchibModelButton.addEventListener('click', domEvents.changeModelValue(schibModel, 'schib'));
changeUserModelButton.addEventListener('click', domEvents.changeModelValue(userModel));

