export const is = {
  string(object) {
    return Object.prototype.toString.call(object) === '[object String]';
  },
  object(object) {
    return Object.prototype.toString.call(object) === '[object Object]';
  },
  notExisting(value) {
    return value === undefined || Object.prototype.toString.call(value) === '[object Null]';
  },
};

export function getObjectValueByDotNotation(dotNotation, object) {
  return dotNotation.split('.')
    .reduce((object, key) => {
      if (object) {
        return object[key];
      }

      return object;
    }, object);
}