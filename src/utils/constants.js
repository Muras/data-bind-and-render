export const bindableAttributes = ['bindSrc', 'bindAlt', 'bindTitle', 'bindHref'];

export const datasetToAttributesMap = {
  bindSrc: 'src',
  bindAlt: 'alt',
  bindTitle: 'title',
  bindHref: 'href',
};
