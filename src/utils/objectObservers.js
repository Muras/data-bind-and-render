import { is } from './dataHelpers';

export function onObjectChange(object, onChange) {
  Object.keys(object).forEach(key => {
    if (is.object(object[key])) {
      onObjectChange(object[key], onChange);
    }

    let internalValue = object[key];
    Object.defineProperty(object, key, {
      get() {
        return internalValue;
      },
      set(newValue) {
        internalValue = newValue;

        onChange && onChange(newValue);
      },
    })
  })
}
