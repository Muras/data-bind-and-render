const getRandomNumber = () => Math.floor(Math.random() * 100);

export default {
  changeModelValue: (model, type) => (event) => {
    event.preventDefault();

    if (type === `schib`) {
      model.test.label = `New value baby! ${getRandomNumber()}`;      
      model.x.y.z = `New prop baby! ${getRandomNumber()}`;      
    } else {
      model.title = `Best Title ${getRandomNumber()}`;

      model.personalData.fullName = `Jan Kowalski ${getRandomNumber()}`;
      model.address.street = `Space! ${getRandomNumber()}`;
      model.address.contact.tel = `7777666123 ${getRandomNumber()}`;
    }
  },
}
