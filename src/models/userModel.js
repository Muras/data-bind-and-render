export default {
  title: '',
  personalData: {
    fullName: 'Michal Murawski',
  },
  address: {
    street: 'Krausego 11/30',
    contact: {
      tel: '509-462-396',
    },
  },
};
