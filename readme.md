
# One-way data-binding

  (live example: http://data-binding-one.surge.sh/)

### Description

As stated in the assignment, I created a JavaScript object's watcher which binds specific object with a provided template. Watcher triggers custom event ('view_update') when we are setting new value to current parameters. This way our element is able to update itself without using any additional actions, methods after binding - we just need to attack returned element to our DOM.

I provided working version for a presented example, and was not thinking much about strange/other cases.

* HTML attribute is created when attribute does not exist,

* If property does not exist on the model we are not adding it to the tag

* Unless it is an empty string

* DataBinder and utils methods are everything we need for the specific functionality. Other files are related to the functionality presentation.

### Developing process

At the beginning I was struggling with a document.createRange().createContextualFragment() API. As I was not able to get a reference to the actual DOM element inside the events callback. That was the reason why I switched to document.createElement() and am returning node.children[0].

I had some issues with mocking document methods and with rendering templates inside tests with proper values. I wasted quite a lot of time trying to configure that.

Cherry on a cake: inside onObjectChange method, while defining setter I forgot to firstly reassign internalValue and then call onChange callback - this mis-step cost me a lot of time and effort and is the reason why I did not manage to prepare more accurate tests.

### Local

In order to run project locally:

1. Please run `yarn install.`
2. Run `yarn test --coverage` in order to run test suits with coverage report.
3. Run `yarn dev` to start webpack-dev-server or `yarn build` in order to build a project.

 ### How to test

We have a view of a card with two different templates inside it: user and schib.
Those two templates are binded with models and those models can be changed by click on a specific buttons presented on the page. Buttons simply call a method which overrides properties of a model. 

### Structure
Inside lib/DataBinder.js we have a logic which follows our data-binding process. One class with few simple methods which are usefull only within this specific class.
I moved some more common functionalities into utils folder where we are holding smaller functions or contants like bindable attributes.



#### Greetings, Michal Murawski
