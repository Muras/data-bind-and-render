import { onObjectChange } from '../../src/utils/objectObservers';

describe('objectObservers "onObjectChange" method tests', () => {
  test('Should not change root object', () => {
    const object = {
      name: 'test',
      data: {
        size: 20,
        other: {
          test: 1,
          color: 'red',
        },
      },
    };
    const copy = JSON.parse(JSON.stringify(object));
    onObjectChange(object);

    expect(copy).toEqual(object);
  });

  test('Should call callback on property set', () => {
    const object = {
      name: 'test',
      data: {
        size: 20,
        other: {
          test: 1,
          color: 'red',
        },
      },
    };
    const onChangeCallback = jest.fn();
    onObjectChange(object, onChangeCallback);

    expect(onChangeCallback).toHaveBeenCalledTimes(0);

    object.name = 'New name';

    expect(onChangeCallback).toHaveBeenCalledTimes(1);
  });
});
