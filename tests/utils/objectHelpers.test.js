import { is, getObjectValueByDotNotation } from '../../src/utils/dataHelpers';

describe('objectHelpers "is" methods tests', () => {
  test('is.notExisting tests', () => {
    expect(is.notExisting('')).toBe(false);
    expect(is.notExisting('asd')).toBe(false);
    expect(is.notExisting(null)).toBe(true);
    expect(is.notExisting(undefined)).toBe(true);
    expect(is.notExisting(() => {})).toBe(false);
    expect(is.notExisting({})).toBe(false);
  });

  test('is.object tests', () => {
    expect(is.object('')).toBe(false);
    expect(is.object('asd')).toBe(false);
    expect(is.object(null)).toBe(false);
    expect(is.object(() => {})).toBe(false);
    expect(is.object({})).toBe(true);
  })
});

describe('objectHelpers "getObjectValueByDotNotation" method tests', () => {
  const object = {
    x: { y: { z: 'some nested property' } },
  };

  test('Should return proper value as an object', () => {
    const result = getObjectValueByDotNotation('x.y', object);

    expect(result).toEqual(object.x.y);
  });

  test('Should return proper value as a string', () => {
    const result = getObjectValueByDotNotation('x.y.z', object);

    expect(result).toEqual(object.x.y.z);
  });

  test('Should return proper undefined for not existing property', () => {
    const result = getObjectValueByDotNotation('x.y.z.a.d', object);

    expect(result).toEqual(undefined);
  });
});