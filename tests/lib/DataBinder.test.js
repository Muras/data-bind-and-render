import 'jest-dom/extend-expect';
import DataBinder from '../../src/lib/DataBinder';
import schibTemplate from '../../src/templates/schibTemplate';
import schibModel from '../../src/models/schibModel';


describe('DataBinder.js', () => {
  let dataBinder;

  beforeEach(() => {
    dataBinder = new DataBinder();
  });

  test('Should export DataBinder class.', () => {
    expect(dataBinder).toBeInstanceOf(DataBinder);
  });

  test('Should return node element with proper values for schibTemplate.', () => {
    const view = dataBinder.generateView(schibModel, schibTemplate);

    expect(view.innerText).toEqual('Schibsted');
    expect(view.getAttribute('title')).toEqual('some nested property to bind');
  });
});
