import domEvents from '../src/domEvents';
import schibModel from '../src/models/schibModel';
import userModel from '../src/models/userModel';

describe('domEvents "changeModelValue" tests', () => {
  test('Should change schibModel', () => {
    const testObject = JSON.parse(JSON.stringify(schibModel));
    const event = domEvents.changeModelValue(testObject, 'schib');

    expect(testObject).toEqual(schibModel);

    event({ preventDefault:() => {} });
    expect(testObject).not.toEqual(schibModel);
  });

  test('Should change userModel', () => {
    const testObject = JSON.parse(JSON.stringify(userModel));
    const event = domEvents.changeModelValue(testObject);

    expect(testObject).toEqual(userModel);

    event({ preventDefault:() => {} });
    expect(testObject).not.toEqual(userModel);
  });
});
